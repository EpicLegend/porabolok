//= template/jquery.min.js
//= template/popper.min.js
//= template/bootstrap.min.js
//= template/wow.min.js

//= template/jquery.fancybox.min.js
//= template/perfect-scrollbar.min.js
//= template/slick.min.js

//= template/custom-scripts.js

//= ../node_modules/lazysizes/lazysizes.js

$(document).ready(function () {
	const ALS = "<button type='button' class='slick-prev'><i class='icon-arrow-left'></i></button>";
	const ARS = "<button type='button' class='slick-next'><i class='icon-arrow-right'></i></button>";


	/*----------------------------
	START COMPONENT picker-number( Cart Plus Minus Button )
	------------------------------ */

	(function(){
		/*
			- picker-number кол-во элемента с кнопками + и -
			- проверка если nextVal == 0, то result(input.value) = 0
			- создание и вызов события change ( для ModX компонента)
		*/
		const numberPickers = document.querySelectorAll(".picker-number");

		numberPickers.forEach(function(item, index) {
			const buttons = item.querySelectorAll(".picker-number__button");

			// Событие на изменение значений
			// с помощью кнопок
			buttons.forEach(function(el) {
				el.addEventListener("click", function () {
					const target = this.parentNode.querySelector(".picker-number__input");
					const oldValue = target.value;
					let newValue;
					const maxValue = parseInt( target.getAttribute("max") );

					if ( this.classList.contains("picker-number__button_inc") ) {
						
						if ( maxValue ) {
							if (oldValue < maxValue) {
								newValue = parseInt( oldValue ) + 1;
							} else {
								newValue = maxValue;
							}
						} else {
							newValue = parseInt( oldValue ) + 1;
						}

					} 
					else if ( this.classList.contains("picker-number__button_dec") ) {		

						if (oldValue > 0) {
							newValue = parseInt( oldValue ) - 1;
						} else {
							newValue = 0;
						}

					}

					target.value = newValue;
					var event = document.createEvent('HTMLEvents');
					event.initEvent('change', true, true);
					target.dispatchEvent(event);
				});

			});

		});

		// Событие на изменение значений
		// ввода в input(с помощью клавиатуры)
		numberPickers.forEach(function(item) {
			
			item.querySelector(".picker-number__input").addEventListener("input", function (e) {

				const minValue = parseInt( this.getAttribute("min") );
				const maxValue = parseInt( this.getAttribute("max") );

				this.value = parseInt(this.value)

				if (maxValue) {

					if ( this.value >= maxValue) {
						this.value = maxValue;
					}

				}

			});

		});

	})();


	/*----------------------------
	END COMPONENT picker-number( Cart Plus Minus Button )
	------------------------------ */

	//=== Testimonials Carousel ===//
	$('.testi-caro').slick({
		arrows: true,
		prevArrow: ALS,
		nextArrow: ARS,
		dots: false,
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		fade: false,
		speed: 1000,
		draggable: true,
		responsive: [
			{
				breakpoint: 981,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 851,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 576,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});

	//===== Shop Detail Carousel =====//
	$(".tabproekt-slider").each(function(index, el) {
		let previewSlider = $(el).find(".shop-detail-caro");
		let navSlider = $(el).find(".shop-detail-nav-caro");
		let currentSlider = $(el).slick({
			arrows: true,
			prevArrow: ALS,
			nextArrow: ARS,
			dots: false,
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			fade: true,
			speed: 1000,
			swipe: false,
		});

		$(el).find(".tabproekt-slider_btn-next").on("click", function(e) {
			e.preventDefault();
			currentSlider.slick('slickNext');
		})

		$(previewSlider).slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			prevArrow: ALS,
			nextArrow: ARS,
			fade: false,
			asNavFor: navSlider
		});
	  
		$(navSlider).slick({
			slidesToShow: 7,
			slidesToScroll: 1,
			asNavFor: previewSlider,
			dots: false,
			arrows: false,
			centerMode: true,
			centerPadding: '0px',
			focusOnSelect: true,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 5,
						slidesToScroll: 1,
						infinite: true,
					}
				}
			]
		});
	});

	// resize у слайдера когда переключаем табы
	$('.tabproekt a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		
		$( $(e.target).attr("href") ).find(".tabproekt-slider").slick('setPosition');

		$( $(e.target).attr("href") ).find('.shop-detail-caro').each(function(index, el) {
			$(el).slick('setPosition');
		});
		$( $(e.target).attr("href") ).find('.shop-detail-nav-caro').each(function(index, el) {
			$(el).slick('setPosition');
		});
	})






	$(".view-slider").slick({
		arrows: true,
		prevArrow: ALS,
		nextArrow: ARS,
		dots: false,
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		fade: false,
		speed: 1000,
		draggable: true,
		responsive: [
			{
				breakpoint: 981,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 851,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 576,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});



	$(".team-slider").slick({
		arrows: true,
		prevArrow: ALS,
		nextArrow: ARS,
		dots: false,
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		fade: false,
		speed: 1000,
		draggable: true,
		responsive: [
			{
				breakpoint: 981,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 851,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 576,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});

	$(".slick-doc-file").slick({
		centerMode: false,
		centerPadding: '30px',
		slidesToShow: 4,
		prevArrow: ALS,
		nextArrow: ARS,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					centerMode: true,
					slidesToShow: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					centerMode: true,
					slidesToShow: 1
				}
			}
		]
	});
	$('.roduct-slider-preview').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow: ALS,
		nextArrow: ARS,
		arrows: false,
		fade: true,
		asNavFor: '.roduct-slider-nav',
		responsive: [
			{
				breakpoint: 981,
				settings: {
					arrows: true,
				}
			}
		]
	});
	$('.roduct-slider-nav').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		asNavFor: '.roduct-slider-preview',
		dots: false,
		arrows: false,
		centerMode: false,
		focusOnSelect: true,
		responsive: [
			{
				breakpoint: 981,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 851,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				}
			}
		]
	});

	// $(".btn-phone-modal").on("click", function(e) {
	// 	e.stopPropagation();
	// 	e.preventDefault();
	// });

    
});